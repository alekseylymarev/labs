import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Aleksey Lymarev on 04.09.2015.
 */
public class RegExp {
    @Test
    public void test(){
        boolean result = true;
        boolean pass = isSecure("CO0LaDFGDSFD");
        System.out.println(pass);
        Assert.assertEquals(true, pass);
    }

    @Test
    public void testOnMinLength(){
        boolean result = true;
        boolean pass = isSecure("C00l_");
        Assert.assertEquals(false, pass);
    }

    @Test
    public void testOnSyms(){
        boolean result = true;
        boolean pass = isSecure("C00l_!!!!!");
        Assert.assertEquals(false, pass);
    }


    /**
     * ������� ��������� �� ��������� ����� �� ������������ ��������� ���������
     *      ����������� �������� ������ 7
     *      ������� ���� �� 1 ��������� �����
     *      ������� ������ 1 �����
     *      �������� ������ _
     *
     * @param password - ������
     * @return isSecure - ���������� true ���� ������ �������� ����� false
     */
    public boolean isSecure(String password) {
        boolean isSecure = false;
        Pattern pattern = Pattern.compile("([A-Z]+|[0-9]+|[_]+|[a-z]){8,}");
        Matcher matcher = pattern.matcher(password);
        isSecure = matcher.matches();
        return isSecure;
    }


}
