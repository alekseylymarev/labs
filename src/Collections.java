import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;
import java.util.Stack;

/**
 * Created by Aleksey Lymarev on 04.09.2015.
 */
public class Collections {


    /**
     * ��������� ������
     * @param word - ������
     * @return - ���������� �� ��������
     */
    public String getReversWord(String word){
        String result = "";
        Stack<Character> reversed = new Stack<>();
        for (char symbol: word.toCharArray()){
            reversed.push(symbol);
        }
        Iterator iterator = reversed.iterator();
        while(iterator.hasNext()){
            result += reversed.pop();
        }
        System.out.println(result);
        return  result;
    }

    @Test
    public void test(){
        Assert.assertEquals("abcde", getReversWord("edcba"));
    }
}
